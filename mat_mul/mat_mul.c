#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <xm.h>
#include "pmc.h"
#include <irqs.h>
#include "big_matrix.h"

#define PRINT(...) do { \
        printf("[P%d] ", XM_PARTITION_SELF); \
        printf(__VA_ARGS__); \
} while (0)

#define NCOUNTERS 3

void PartitionMain(void) {

        PRINT("PMU READING STARTS HERE\n");

        initPMU();

        /* Fill the pmu registers in order to count the desired events */
        allocCounterPMU( 0, ARMV7_A9_INST_EXEC);
        allocCounterPMU( 1, ARMV7_PERFCTR_CLOCK_CYCLES); 
        allocCounterPMU( 2, ARMV7_PERFCTR_MEM_READ);


        int r;
        for (r = 0; r<= 2; r++) {
                enablePMR(r);
        }


        /* Fill the matrices with ordered sequence of numbers */
        /* int counter = 0; */
        /* int i = 0; */
        /* while (i < NCOLS) { */
        /*         int j = 0; */
        /*         while (j < NCOLS) { */
        /*                 int random_value; */
        /*                 random_value = counter; */

        /*                 matrix_a[i][j] = random_value; */
        /*                 matrix_b[i][j] = random_value; */

        /*                 j+=1; */
        /*                 counter+=1; */
        /*         } */

        /*         i+=1; */
        /* } */


        /* Compute the matrix multiplication */
        int res_matrix[NCOLS][NCOLS];
        int row_A = 0;
        while (row_A < NCOLS) {
                int col_B = 0;
                while (col_B < NCOLS) {
                        int res = 0;
                        int i;
                        for (i = 0; i < NCOLS; i++) {
                                res += MATRIX_A[row_A][i] * MATRIX_B[i][col_B];
                        }

                        res_matrix[row_A][col_B] = res;

                        col_B += 1;
                }

                row_A += 1;
        }


        /* Print the result matrix */
        int i = 0;
        while (i < NCOLS) {
                int j = 0;
                while (j < NCOLS) {
                        PRINT("%d, %d  ", i, j);
                        PRINT("%d\n", res_matrix[i][j]);
                        j+=1;
                }
                i+=1;
        }
        
        int nInst = readPMR(0);
        int nCycle = readPMR(1);
        int nMemRead = readPMR(2);
        PRINT("Instrucciones ejecutadas: %d\n", nInst);
        PRINT("Ciclos de reloj: %d\n", nCycle);
        PRINT("Lecturas de memoria: %d\n", nMemRead);
        clearAllPMR();
        XM_halt_partition(XM_PARTITION_SELF);
}
