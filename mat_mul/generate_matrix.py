import numpy as np
import sys

N = int(sys.argv[1])

with open("big_matrix.h", "w") as file:
    # First line is the header
    file.write(f"#define NCOLS {N}\n")
    # Second line
    file.write(f"int MATRIX_A[NCOLS][NCOLS] = {{\n")

    for i in range(N):
        for j in range(N):
            random_value = np.random.randint(0, high=256)
            if j == 0:
                file.write(f"    {{ {random_value},")
            elif j == N - 1:
                file.write(f" {random_value} }},\n")
            else:
                file.write(f" {random_value}, ")

    file.write(f"}};\n")


    # Second array 
    file.write(f"int MATRIX_B[NCOLS][NCOLS] = {{\n")

    for i in range(N):
        for j in range(N):
            random_value = np.random.randint(0, high=256)
            if j == 0:
                file.write(f"    {{ {random_value},")
            elif j == N - 1:
                file.write(f" {random_value} }},\n")
            else:
                file.write(f" {random_value}, ")

    file.write(f"}};\n")
