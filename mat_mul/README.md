You need python3:
$ sudo apt install python3
$ sudo apt install python3-pip

Then you need the numpy package:
$ sudo pip install numpy


To generate a random matrix of size NxN:
$ python3 generate_matrix.py N

Then:
$ make clean
$ make

And you can load the resident_sw in the cpu
