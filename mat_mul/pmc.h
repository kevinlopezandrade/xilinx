#include <xm.h>

#define ARMV7_PERFCTR_PMNC_SW_INCR          0x00
#define ARMV7_PERFCTR_L1_ICACHE_REFILL          0x01
#define ARMV7_PERFCTR_ITLB_REFILL           0x02
#define ARMV7_PERFCTR_L1_DCACHE_REFILL          0x03
#define ARMV7_PERFCTR_L1_DCACHE_ACCESS          0x04
#define ARMV7_PERFCTR_DTLB_REFILL           0x05
#define ARMV7_PERFCTR_MEM_READ              0x06
#define ARMV7_PERFCTR_MEM_WRITE             0x07
#define ARMV7_PERFCTR_INSTR_EXECUTED            0x08
#define ARMV7_PERFCTR_EXC_TAKEN             0x09
#define ARMV7_PERFCTR_EXC_EXECUTED          0x0A
#define ARMV7_PERFCTR_CID_WRITE             0x0B

/*
 * ARMV7_PERFCTR_PC_WRITE is equivalent to HW_BRANCH_INSTRUCTIONS.
 * It counts:
 *  - all (taken) branch instructions,
 *  - instructions that explicitly write the PC,
 *  - exception generating instructions.
 */
#define ARMV7_PERFCTR_PC_WRITE              0x0C
#define ARMV7_PERFCTR_PC_IMM_BRANCH         0x0D
#define ARMV7_PERFCTR_PC_PROC_RETURN            0x0E
#define ARMV7_PERFCTR_MEM_UNALIGNED_ACCESS      0x0F
#define ARMV7_PERFCTR_PC_BRANCH_MIS_PRED        0x10
#define ARMV7_PERFCTR_CLOCK_CYCLES          0x11
#define ARMV7_PERFCTR_PC_BRANCH_PRED            0x12

/* These events are defined by the PMUv2 supplement (ARM DDI 0457A). */
#define ARMV7_PERFCTR_MEM_ACCESS            0x13
#define ARMV7_PERFCTR_L1_ICACHE_ACCESS          0x14
#define ARMV7_PERFCTR_L1_DCACHE_WB          0x15
#define ARMV7_PERFCTR_L2_CACHE_ACCESS           0x16
#define ARMV7_PERFCTR_L2_CACHE_REFILL           0x17
#define ARMV7_PERFCTR_L2_CACHE_WB           0x18
#define ARMV7_PERFCTR_BUS_ACCESS            0x19
#define ARMV7_PERFCTR_MEM_ERROR             0x1A
#define ARMV7_PERFCTR_INSTR_SPEC            0x1B
#define ARMV7_PERFCTR_TTBR_WRITE            0x1C
#define ARMV7_PERFCTR_BUS_CYCLES            0x1D

#define ARMV7_PERFCTR_CPU_CYCLES            0xFF

/* ARMv7 Cortex-A9 specific event types */
#define ARMV7_A9_PERFCTR_INSTR_CORE_RENAME      0x68
#define ARMV7_A9_PERFCTR_STALL_ICACHE           0x60
#define ARMV7_A9_PERFCTR_STALL_DISPATCH         0x66

// del manual DDIO388F cortex a9r2p2

#define ARMV7_A9_COHERENT_LINEFILL_MISS  0x50 // Coherent linefill miss
#define ARMV7_A9_STREX_PASSED 0x63   //STREX passed.
#define ARMV7_A9_DATA_EVICTION 0x65 //Data eviction.


#define ARMV7_A9_INST_EXEC 0x68 // Instructions coming out of the core renaming stage
     //Counts the number of instructions going through the Register Renaming stage. 
     //This number is an approximate number of the total number of instructions 
     //speculatively executed, and even more approximate of the total number of 
     //instructions architecturally executed. The approximation depends mainly 
     //on the branch misprediction rate.

#define ARMV7_A9_MAIN_INST_EXEC   0x70   //Main execution unit instructions
#define ARMV7_A9_2nd_INST_EXEC    0x71   //Second execution unit instructions
#define ARMV7_A9_LS_INST_EXEC     0x72   //Load/Store Instructions
#define ARMV7_A9_FP_INST_EXEC     0x73 //Floating-point instructions

#define ARMV7_PERFCTR_PLD_FULL_DEP_STALL_CYCLES   0x80
#define ARMV7_PERFCTR_DATA_WR_DEP_STALL_CYCLES   0x81
#define ARMV7_PERFCTR_ITLB_MISS_DEP_STALL_CYCLES   0x82
#define ARMV7_PERFCTR_DTLB_MISS_DEP_STALL_CYCLES   0x83
#define ARMV7_PERFCTR_MICRO_ITLB_MISS_DEP_STALL_CYCLES   0x84
#define ARMV7_PERFCTR_MICRO_DTLB_MISS_DEP_STALL_CYCLES   0x85
#define ARMV7_PERFCTR_DMB_DEP_STALL_CYCLES   0x86

#define ARMV7_PERFCTR_INTGR_CLK_ENABLED_CYCLES   0x8A
#define ARMV7_PERFCTR_DATA_ENGINE_CLK_EN_CYCLES   0x8B

#define ARMV7_PERFCTR_ISB_INST		   0x90
#define ARMV7_PERFCTR_DSB_INST		   0x91
#define ARMV7_PERFCTR_DMB_INST		   0x92
#define ARMV7_PERFCTR_EXT_INTERRUPTS	   0x93

#define ARMV7_PERFCTR_PLE_CACHE_LINE_RQST_COMPLETED   0xA0
#define ARMV7_PERFCTR_PLE_CACHE_LINE_RQST_SKIPPED   0xA1
#define ARMV7_PERFCTR_PLE_FIFO_FLUSH	   0xA2
#define ARMV7_PERFCTR_PLE_RQST_COMPLETED   0xA3
#define ARMV7_PERFCTR_PLE_FIFO_OVERFLOW	   0xA4
#define ARMV7_PERFCTR_PLE_RQST_PROG	   0xA5

void initPMU();

void allocCounterPMU(xm_u32_t r, xm_u32_t event);

void enablePMR(int counter);

void enableCCNT();

xm_u32_t readPMR(int r);

xm_u32_t readCCNT();

void clearAllPMR();

void clearCCNT();

void disablePMU (void);



