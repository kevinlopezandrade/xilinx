#include <xm.h>
#include "pmc.h"

void initPMU() {

  // Disable all counters (PMCNTENCLR):
  __asm__ __volatile__ ("MCR p15, 0, %0, c9, c12, 2\t\n" :: "r"(0x8000003f));

  xm_u32_t pmcr  = 0x1    // enable counters
            | 0x2    // reset all other counters
            | 0x4    // reset cycle counter
            | 0x8    // enable "by 64" divider for CCNT.
            | 0x10;  // Export events to external monitoring
  // program the performance-counter control-register (PMCR):
  __asm__ __volatile__ ("MCR p15, 0, %0, c9, c12, 0\t\n" :: "r"(pmcr));
  // clear overflows (PMOVSR):
  __asm__ __volatile__ ("MCR p15, 0, %0, c9, c12, 3\t\n" :: "r"(0x8000003f));

  // Enable all counters (PMCNTENSET):
  __asm__ __volatile__ ("MCR p15, 0, %0, c9, c12, 1\t\n" :: "r"(0x8000003f));

    // enable user-mode access to the performance counter
  __asm__ __volatile__ ("MCR p15, 0, %0, C9, C14, 0\n\t" :: "r"(1));

    // disable counter overflow interrupts (just in case)
  __asm__ __volatile__ ("MCR p15, 0, %0, C9, C14, 2\n\t" :: "r"(0x8000000f));
}

void allocCounterPMU(xm_u32_t r, xm_u32_t event) {

  // Write PMXEVTYPER #r
  // This is done by first writing the counter number to PMSELR and then writing PMXEVTYPER
// Select event counter in PMSELR
  __asm__ __volatile__ ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(r));
  // Set the event number in PMXEVTYPER
  __asm__ __volatile__ ("mcr p15, 0, %0, c9, c13, 1\n\t" :: "r"(event)); 
}

void enableCCNT () {
 __asm__ __volatile__("MOV r0, #0x80000000\n\t"  \
                      "MCR p15, 0, r0, c9, c12, 1\n\t");
}

void enablePMR(int counter){
    __asm__ __volatile__(   "MOV     r1, #0x1\n\t");
    __asm__ __volatile__(   "MOV     r1, r1, LSL %[counter]\n\t" :: [counter] "r" (counter));
     //Write PMCNTENSET Register
    __asm__ __volatile__(   "MCR     p15, 0, r1, c9, c12, 1\n\t");      
}

xm_u32_t readPMR(int r) {
  // This is done by first writing the counter number to PMSELR and then reading PMXEVCNTR
  xm_u32_t value;
// Select event counter in PMSELR
  __asm__ __volatile__ ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(r));  

// Read from PMXEVCNTR
  __asm__ __volatile__ ("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r"(value));  

  return value;
}


xm_u32_t readCCNT() {
  xm_u32_t value;
  // Read CPU cycle count from the CCNT Register
  __asm__ __volatile__  ("mrc p15, 0, %0, c9, c13, 0\t\n": "=r"(value));
  return value;
}

void clearAllPMR() {
    __asm__ __volatile__("MRC     p15, 0, r0, c9, c12, 0\n\t"
                    "ORR     r0, r0, #0x02\n\t"
                    "MCR     p15, 0, r0, c9, c12, 0\n\t"
    );
}

void clearCCNT() {
    __asm__ __volatile__("MRC     p15, 0, r0, c9, c12, 0\n\t" 
                    "ORR     r0, r0, #0x04\n\t"
                    "MCR     p15, 0, r0, c9, c12, 0\n\t"
    );
}

void disablePMU (void){
    __asm__ __volatile__(   "MRC     p15, 0, r0, c9, c12, 0\n\t"
                    "BIC     r0, r0, #0x01\n\t"
                    "MCR     p15, 0, r0, c9, c12, 0\n\t"
    );
}



