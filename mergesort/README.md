You need python3:
$ sudo apt install python3
$ sudo apt install python3-pip

Then you need the numpy package:
$ sudo pip install numpy


To generate a random array of size N:
$ python3 generate_array.py N

Then:
$ make clean
$ make

And you can load the resident_sw in the cpu
