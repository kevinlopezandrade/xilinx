import numpy as np
import sys

N = int(sys.argv[1])

with open("big_array.h", "w") as file:
    # First line is the header
    file.write(f"#define NCOLS {N}\n")
    # Second line
    file.write(f"int ARRAY[NCOLS] = {{\n")

    for i in range(N):
        random_value = np.random.randint(0, high=256)
        file.write(f"    {random_value},\n")

    file.write(f"}};\n")
