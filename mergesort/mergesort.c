#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <xm.h>
#include "pmc.h"
#include <irqs.h>
#include "big_array.h"

#define PRINT(...) do { \
        printf("[P%d] ", XM_PARTITION_SELF); \
        printf(__VA_ARGS__); \
} while (0)

#define NCOUNTERS 3

/* void mergesort(int array[NCOLS], int start, int end); */
/* void merge(int array[NCOLS], int start_a, int end_a, int start_b, int end_b); */

void imsort(int* xs, int l, int u);
void swap(int* xs, int i, int j);
void wmerge(int* xs, int i, int m, int j, int n, int w);
void wsort(int* xs, int l, int u, int w);

void PartitionMain(void) {

        PRINT("PMU READING STARTS HERE\n");
        PRINT("BEFORE SORTING\n");

        int i;
        for (i = 0; i < NCOLS; i++) {
                PRINT("%d\n", ARRAY[i]);
        }

        initPMU();

        /* Fill the pmu registers in order to count the desired events */
        allocCounterPMU( 0, ARMV7_A9_INST_EXEC);
        allocCounterPMU( 1, ARMV7_PERFCTR_CLOCK_CYCLES); 
        allocCounterPMU( 2, ARMV7_PERFCTR_MEM_READ);


        int r;
        for (r = 0; r<= 2; r++) {
                enablePMR(r);
        }


        /* Compute Mergesort on RANDOM ARRAY*/
        imsort(ARRAY, 0, NCOLS);


        /* Read information from the PMU */
        
        int nInst = readPMR(0);
        int nCycle = readPMR(1);
        int nMemRead = readPMR(2);

        clearAllPMR();

        /* Print the sorted array */
        PRINT("SORTED ARRAY\n");
        for (i = 0; i < NCOLS; i++) {
                PRINT("%d\n", ARRAY[i]);
        }

        PRINT("Instrucciones ejecutadas: %d\n", nInst);
        PRINT("Ciclos de reloj: %d\n", nCycle);
        PRINT("Lecturas de memoria: %d\n", nMemRead);
        XM_halt_partition(XM_PARTITION_SELF);
}

void swap(int* xs, int i, int j) {
    int tmp = xs[i]; xs[i] = xs[j]; xs[j] = tmp;
}

void wmerge(int* xs, int i, int m, int j, int n, int w) {
    while (i < m && j < n)
        swap(xs, w++, xs[i] < xs[j] ? i++ : j++);
    while (i < m)
        swap(xs, w++, i++);
    while (j < n)
        swap(xs, w++, j++);
}

/* 
 * sort xs[l, u), and put result to working area w. 
 * constraint, len(w) == u - l
 */
void wsort(int* xs, int l, int u, int w) {
    int m;
    if (u - l > 1) {
        m = l + (u - l) / 2;
        imsort(xs, l, m);
        imsort(xs, m, u);
        wmerge(xs, l, m, m, u, w);
    }
    else
        while (l < u)
            swap(xs, l++, w++);
}

void imsort(int* xs, int l, int u) {
    int m, n, w;
    if (u - l > 1) {
        m = l + (u - l) / 2;
        w = l + u - m;
        wsort(xs, l, m, w); /* the last half contains sorted elements */
        while (w - l > 2) {
            n = w;
            w = l + (n - l + 1) / 2;
            wsort(xs, w, n, l);  /* the first half of the previous working area contains sorted elements */
            wmerge(xs, l, l + n - w, n, u, w);
        }
        for (n = w; n > l; --n) /*switch to insertion sort*/
            for (m = n; m < u && xs[m] < xs[m-1]; ++m)
                swap(xs, m, m - 1);
    }
}


/* void mergesort(int array[NCOLS], int start, int end) { */

/*         /1* Caso base *1/ */
/*         if (start - end == 0) { */
/*                 return; */
/*         } */
/*         else { */
/*                 int start_a; */
/*                 int end_a; */
/*                 start_a = start; */
/*                 end_a = (int) (start + end)/2; */

/*                 int start_b; */
/*                 int end_b; */
/*                 start_b = end_a + 1; */
/*                 end_b = end; */

/*                 mergesort(array, start_a, end_a); */
/*                 mergesort(array, start_b, end_b); */

/*                 merge(array, start_a, end_a, start_b, end_b); */
/*         } */
/* } */

/* void merge(int array[NCOLS], int start_a, int end_a, int start_b, int end_b){ */
/*         int current = start_a; */

/*         int index_a = start_a; */
/*         int index_b = start_b; */
/*         int aux; */
/*         int auxIndex; */

/*         while ((index_a <= end_a) || (index_b <= end_b)) { */
/*                 if (array[index_a] <= array[index_b]) { */
/*                         //array[current] = array[index_a]; */
/*                         //current += 1; */
/*                         index_b += 1; */
/*                 } */
/*                 else { */
/*                         aux = array[index_b]; */
/*                         auxIndex = index_b; */
/*                         while(auxIndex != index_a){ */
/*                                 array[auxIndex] = array[auxIndex--] */
/*                         } */
/*                         array[index_a] = aux; */
/*                         index_a += 1; */
/*                 } */
/*         } */

/*         if (index_a < end_a) { */
/*                 while current <= end_a { */
/*                         array[current] = */ 
/*                 } */
/*         } */

/*         if (index_b < end_b) { */
/*         } */

/* } */
